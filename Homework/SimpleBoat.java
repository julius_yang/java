/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleboat;

/**
 *
 * @author julius
 */
public class SimpleBoat {
    public static void main (String[] args) {
        Boat boat = new Boat();
        boat.goFast();
        // Shouldn't we run whereIsTheSail here too?
        // Homework just says run  the three methods in order...
        boat.goSlow();
        boat.whereIsTheSail();
    }
}
