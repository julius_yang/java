/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleboat;

/**
 *
 * @author julius
 */
public class Boat {
    
    // Instance variables
    private String name;
    private boolean sailUp;
    
    // Constructor
    public Boat(String nameArg) {
        this.setName(nameArg);
    }
    
    // Private methods (set info)
    private void goFast() {
        sailUp = true;
    }
    
    private void goSlow() {
        sailUp = false;
    }
    
    private void setName(String argName) {
        name = argName;
        char firstLetter = name.charAt(0);
        switch (firstLetter) {
            case 'B':
                this.goFast();
                break;
            case 'C':
                this.goFast();
                break;
            case 'N':
                this.goFast();
                break;
            default:
                this.goSlow();
                break;
        }
    }
    
    // Public methods (only fetch info)
    public String getName() {
        return name;
    }
    public String whereIsTheSail() {
        String sailState = sailUp ? "up" : "down";
        return "sail is " + sailState;
    }
}
