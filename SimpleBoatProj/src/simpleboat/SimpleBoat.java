/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleboat;

/**
 *
 * @author julius
 */
public class SimpleBoat {
    public static void main (String[] args) {
        
        //Declare an array of boats of same length s arguments
        Boat[] boats = new Boat[args.length];
        
        for (int i = 0; i < args.length; i++) {
            // We know the arguments are always strings which
            // are legal arguments to the Boat constructor
            boats[i] = new Boat(args[i]);
            
            System.out.println( boats[i].getName());
            System.out.println( boats[i].whereIsTheSail() );
            
        }
    }
    
}
