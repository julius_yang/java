/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juliusyang;

/**
 *
 * @author julius
 */
public class Almond extends Nut {

    public static final char SYMBOL = 'A';
    public static final int NUTRITION_POINTS = 5;
    
    Almond() {
        symbol = Almond.SYMBOL;
        this.create();
    }

    @Override
    public char getSymbol() {
        return symbol;
    }
    
    @Override
    public void create() {
        super.create();
    }
}
