/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juliusyang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author julius
 */
public class HungrySquirrel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Maze maze = new Maze("Maze.txt");
        maze.display();
        Squirrel squirrel = new Squirrel();
        maze.display();

        // Now accept input from the user and move the squirrel.
        boolean gameOn = true;

        String command = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        //program waits until input is provided
        while (gameOn) {
            System.out.println("Enter command (uldr or quit)");
            try {
                command = br.readLine();
            } catch (IOException e) {
                System.out.println("Could not understand command");
                System.exit(1);
            }

            if (command != null && command.equals("quit")) {
                System.out.println("Bye!");
                System.exit(0);
            }

            // Parse first letter of command and see if it is uldr
            squirrel.moveDirection(command);
            int totalNuts = squirrel.getTotalNutsCollected();

            if (totalNuts == Nut.TOTAL_NUTS) {
                maze.display();
                System.out.println("You collected all the nuts!");
                System.out.println("Your final score is: " + (squirrel.getHealth() + squirrel.getPoints()));
                System.exit(0);
            }
            
            int currentHealth = squirrel.getHealth();

            if (squirrel.getHealth() <= 0) {
                System.out.println("Squirrel's health is too low!  It died.");
                System.exit(0);
            }

            int totalPoints = squirrel.getPoints();
            String status = "Squirrel health: " + currentHealth
                    + " points: " + totalPoints
                    + " total nuts: " + totalNuts;

            maze.display();

            System.out.println(status);
        }
    }
}
