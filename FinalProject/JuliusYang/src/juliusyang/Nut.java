/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juliusyang;

/**
 *
 * @author julius
 */
public abstract class Nut extends Entity {
    
    public static final int TOTAL_NUTS = 5;
    public static final int PEANUT_POINTS = 10;
    public static final int ALMOND_POINTS = 5;
    
    // Do we need a create method here to randomly generate
    // an almond or peanut?
    @Override
    public void create() {
        super.create();
    }
    
    static public int isNuts(char symbol) {
        
        switch (symbol) {
            case Almond.SYMBOL:
                return Almond.NUTRITION_POINTS;
            case Peanut.SYMBOL:
                return Peanut.NUTRITION_POINTS;
            default:
                return 0;
        }
    }
}

