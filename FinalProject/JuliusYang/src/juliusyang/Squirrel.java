/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juliusyang;

import java.util.Scanner;
import static juliusyang.Maze.available;

/**
 *
 * @author julius
 */
public class Squirrel extends Entity {

    public static final char SYMBOL = '@';
    private int health = 50;
    // The total nut value collected.
    private int points = 0;
    // Total nuts collected.
    private int totalNuts = 0;

    Squirrel() {
        symbol = Squirrel.SYMBOL;
        // We don't call Entity.create() because
        // it places the entity randomly.
        this.create();
    }

    @Override
    public char getSymbol() {
        return symbol;
    }

    // The squirrel's create method asks the user to
    // place the squirrel.  The Entity.create() method
    // places the entity randomly, so we need to override.
    @Override
    final public void create() {

        int[] location = new int[2];

        do {

            Scanner keyboard = (new Scanner(System.in)).useDelimiter(",|\n");
            System.out.println("Enter valid starting squirrel location (x,y):");
            for (int i = 0; i < location.length; i++) {
                // Returns true if there is a next token
                String input = keyboard.next();
                String trim = input.trim();

                if (trim.equals("quit")) {
                    System.out.println("Bye!");
                    System.exit(0);
                }

                // TODO Should check for bad input
                location[i] = Integer.parseInt(trim);
            }

            // It would be nice to display an error message when
            // the user chooses an invalid location.
        } while (!available(location));

        this.move(this.xPos, this.yPos, location[0], location[1], this.getSymbol());

    }

    public char moveDirection(String command) {
        char[] chars = command.toCharArray();

        // Can't do anything without a command
        if (chars.length == 0) {
            return Maze.WALL_CHAR;
        }

        char c = chars[0];
        switch (c) {
            case 'u':
                return moveUp();
            case 'd':
                return moveDown();
            case 'l':
                return moveLeft();
            case 'r':
                return moveRight();
            default:
                break;
        }

        return Maze.WALL_CHAR;
    }

    @Override
    public char move(int startX, int startY, int endX, int endY, char symbol) {
        if (Maze.mazeData[endY][endX] == Maze.WALL_CHAR) {
            // Don't do anything.
            return Maze.WALL_CHAR;
        } else {
            // Every move diminishes squirrel health by 1.
            health--;

            char destinationTerrain = Maze.mazeData[endY][endX];
            xPos = endX;
            yPos = endY;

            // If the starting location was not a wall symbol, then
            // it should now be a space symbol.  The squirrel is the
            // only thing that moves, and it automatically "picks up"
            // any other symbol.
            if (Maze.mazeData[startY][startX] != Maze.WALL_CHAR) {
                Maze.mazeData[startY][startX] = Maze.SPACE_CHAR;
            }
            Maze.mazeData[endY][endX] = this.getSymbol();

            addTerrainPoints(destinationTerrain);

            // Return the symbol that used to be here.
            return destinationTerrain;
        }
    }

    public int getTotalNutsCollected() {
        return totalNuts;
    }

    public int getHealth() {
        return health;
    }

    public int getPoints() {
        return points;
    }

    // When the squirrel moves onto a nut, change its stats.
    private void addTerrainPoints(char terrain) {

        int value = Nut.isNuts(terrain);
        points += value;
        health += value;
        if ( value != 0 ) {
            System.out.println("You collected a nut worth " + value + " points.");
            totalNuts++;
        }
    }

    public char moveLeft() {
        char terrain = this.move(this.xPos, this.yPos, this.xPos - 1, this.yPos, this.getSymbol());
        return terrain;
    }

    public char moveRight() {
        char terrain = this.move(this.xPos, this.yPos, this.xPos + 1, this.yPos, this.getSymbol());
        return terrain;
    }

    public char moveDown() {
        char terrain = this.move(this.xPos, this.yPos, this.xPos, this.yPos - 1, this.getSymbol());
        return terrain;
    }

    public char moveUp() {
        char terrain = this.move(this.xPos, this.yPos, this.xPos, this.yPos + 1, this.getSymbol());
        return terrain;
    }
}
