/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juliusyang;

/**
 *
 * @author julius
 */
public class Peanut extends Nut {

    public static final char SYMBOL = 'P';
    public static final int NUTRITION_POINTS = 10;
    
    Peanut() {
        symbol = Peanut.SYMBOL;
        this.create();
    }

    @Override
    public char getSymbol() {
        return symbol;
    }
    
    @Override
    public void create() {
        super.create();
    }
}
