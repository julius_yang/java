/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juliusyang;

import static juliusyang.Maze.randomLocation;
import static juliusyang.Maze.available;

/**
 *
 * @author julius
 */
public abstract class Entity implements EntityInterface {
    
    char symbol;
    int xPos = 0;
    int yPos = 0;

    @Override
    public abstract char getSymbol();

    @Override
    public void create() {
        this.placeRandomly();
    }
    
    @Override
    public char move(int startX, int startY, int endX, int endY, char symbol) {
        if (Maze.mazeData[endY][endX] == Maze.WALL_CHAR) {
            // Don't do anything.
            return Maze.WALL_CHAR;
        } else {
            char preMoveSymbol = Maze.mazeData[endY][endX];
            xPos = endX;
            yPos = endY;

            // If the starting location was not a wall symbol, then
            // it should now be a space symbol.  The squirrel is the
            // only thing that moves, and it automatically "picks up"
            // any other symbol.
            if (Maze.mazeData[startY][startX] != Maze.WALL_CHAR) {
                Maze.mazeData[startY][startX] = Maze.SPACE_CHAR;
            }
            Maze.mazeData[endY][endX] = this.getSymbol();
            
            // Return the symbol that used to be here.
            return preMoveSymbol;
        }
    }
    
    // Returns the character that was replaced
    void placeRandomly() {

        int[] startLocation = randomLocation();

        // Small risk of infinite loop here, if there
        // are more entities than open space.
        while (!available(startLocation)) {
            startLocation = randomLocation();
        }
        int x = startLocation[0];
        int y = startLocation[1];

        Maze.mazeData[y][x] = this.getSymbol();
        this.xPos = x;
        this.yPos = y;
    }

}
