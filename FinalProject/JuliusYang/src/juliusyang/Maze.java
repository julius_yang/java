/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juliusyang;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author julius
 */
public class Maze {

    static int MAX_MAZE_ROW = 20;
    static int MAX_MAZE_COLUMN = 50;
    static char WALL_CHAR = '*';
    static char SPACE_CHAR = ' ';
    static int NUM_NUTS = 5;
    // This will store the maze as column, row.
    // static char[][] maze;
    // This will store the maze as row, column for ease of
    // reading and displaying.
    static char[][] mazeData = new char[MAX_MAZE_ROW][MAX_MAZE_COLUMN];

    // Constructor
    Maze(String file) {
        create(file);

        // Now populate it with entities
        populate();
    }

    private void create(String file) {

        try {
            File source = new File(file);
            FileReader fr = new FileReader(source);
            BufferedReader in = new BufferedReader(fr);
            String line;

            // Start reading the file, which means read
            // the top line.  Note that we are assigning
            // the origin 0,0 to the lower left corner of
            // the maze, which makes the indexing a little
            // less intuitive.
            int i = MAX_MAZE_ROW - 1;

            // We should either read only the first 20 lines
            // and 50 chars of each line, or pre-process the
            // text file and detect whether it is not 50x20
            // and exit.  But for now assume the input file is
            // 50x20.
            while ((line = in.readLine()) != null) {
                
                // Read the line as an array of char
                char[] lineChars = line.toCharArray();
                mazeData[i] = lineChars;
                i--;
                if (i < 0) {
                    break;
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void populate() {
        makeNuts();
        //makeSquirrel();
    }

    private void makeSquirrel() {
        // Show the user the location of the nuts
        this.display();

        Squirrel squirrel = new Squirrel();

        // Show the user where the squirrel winds up
        this.display();
    }

    private void makeNuts() {

        Nut[] nuts = new Nut[Nut.TOTAL_NUTS];

        for (int i = 0; i < nuts.length; i++) {
            double val = Math.random();
            nuts[i] = val < .5 ? new Almond() : new Peanut();
        }
    }

//    private char[][] transposeArray(char input[][]) {
//
//        // Who doesn't love stackoverflow.com?
//        int width = input.length;
//        int height = input[0].length;
//
//        char[][] array_new = new char[height][width];
//
//        for (int x = 0; x < width; x++) {
//            for (int y = 0; y < height; y++) {
//                array_new[y][x] = input[x][y];
//            }
//        }
//        return array_new;
//    }

    void display() {
        String line;
        for (int row = MAX_MAZE_ROW - 1; row >= 0; row--) {
            line = new String(mazeData[row]);
            System.out.println(line);
        }
        System.out.println(System.getProperty("line.separator"));
    }

    // There's more elegant ways to do this :(
    static boolean available(int x, int y) {
        char mazeSymbol = mazeData[y][x];

        if (mazeSymbol == Maze.WALL_CHAR
                || mazeSymbol == Almond.SYMBOL
                || mazeSymbol == Peanut.SYMBOL
                || mazeSymbol == Squirrel.SYMBOL) {
            return false;
        }
        return true;
    }

    static boolean available(int[] input) {
        return available(input[0], input[1]);
    }

    static int[] randomLocation() {
        int y = (int) (Maze.MAX_MAZE_ROW * Math.random());
        int x = (int) (Maze.MAX_MAZE_COLUMN * Math.random());
        int[] result = new int[2];
        result[0] = x;
        result[1] = y;
        return result;
    }
}
