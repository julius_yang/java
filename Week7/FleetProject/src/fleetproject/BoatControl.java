/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author Julius
 */
public interface BoatControl {
    void powerOn();
    void powerOff();
    boolean isPowered();
    void turnRight();
    void turnLeft();
    void goFaster();
    void goSlower();
    String whatIsBoatState();
}
