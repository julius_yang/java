/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author Julius
 */
public class RaceBoat extends Boat {
        
    // variables
    
    // constructors
    RaceBoat (String name, String id) {
        super(name, id);
    }

    RaceBoat (String name) {
        super(name);
    }
    
    // private methods
    
    // public methods
    
    // Too lazy to figure out how to retrieve name of class and
    // format it properly (this.getClass().getName() returns
    // the package too)
    @Override
    String getType() {
        return "Race Boat";
    }
    
    public void throttleForward() {
        boatState = "Throttle Forward";
    }
    
    @Override
    public void turnRight() {
        normalizeHeading();
        heading += 10;
    }
    
    @Override
    public void turnLeft() {
        normalizeHeading();
        heading -= 10;
    }
    
    @Override
    public void goFaster() {
        speed = speed + 2;
        throttleForward();
    }
    
    @Override
    public void goSlower() {
        speed = speed - 2;
        throttleBack();
    }
    

    public void throttleBack() {
        boatState = "Throttle Back";
    }
    
    @Override
    public String whatIsBoatState() {
        return boatState;
    }
}
