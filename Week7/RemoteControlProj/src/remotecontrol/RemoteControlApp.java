/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package remotecontrol;

/**
 *
 * @author julius
 */
public class RemoteControlApp {

    public static void main(String[] args) {

        RemoteControl device;

        for (int i = 0; i < 2; i++) {
            if (Math.random() < 0.5) {
                device = new TV("Sony", 28);
            } else {
                device = new VCR("JVC");
            }

            device.powerOnOff();
            device.volumeUp(10);
            device.volumeDown(5);
            device.mute();
            device.powerOnOff();
        }
    }
}
