/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package remotecontrol;

/**
 *
 * @author julius
 */
public class TV extends Device {
    private String make;
    private int screenSize;
    private boolean power;
    private int volume;
    
    public TV(String make, int screenSize) {
        this.make = make;
        this.screenSize = screenSize;
        
    }
    
    @Override
    public boolean powerOnOff() {
        power = !power;
        System.out.println(make + " " + screenSize + " inch TV power " + ( power ? "on." : "off.") );
        return power;
    }
    
    @Override
    public int volumeUp(int increment) {
        if (!power) {
            return 0;
        }
        
        volume += increment;
        volume = Math.min(volume, MAX_VOLUME);
        System.out.println(make + " " + screenSize + " inch TV volume level: " + volume);
        return volume;
    }
    
    @Override
    public int volumeDown(int decrement) {
        if (!power) {
            return 0;
        }
        
        volume -= decrement;
        volume = Math.max(volume, MIN_VOLUME);
        System.out.println(make + " " + screenSize + " inch TV volume level: " + volume);
        return volume;
    }
    
    @Override
    public void mute() {
        volume = MIN_VOLUME;
        System.out.println(make + " " + screenSize + " inch TV is muted");
    }
}
