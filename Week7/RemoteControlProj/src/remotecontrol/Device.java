/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package remotecontrol;

/**
 *
 * @author julius
 */
public abstract class Device implements RemoteControl {
    
    private String make;
    private boolean power;
    private int volume;
    
    @Override
    public boolean powerOnOff() {
        
        power = !power;
        return power;
    }
    
    @Override
    public int volumeUp(int increment) {
        
        if (!power) {
            return 0;
        }
        
        volume += increment;
        volume = Math.min(volume, MAX_VOLUME);
        return volume;
    }
    
    @Override
    public int volumeDown(int decrement) {
        
        if (!power) {
            return 0;
        }
        
        volume -= decrement;
        volume = Math.max(volume, MIN_VOLUME);
        return volume;
    }
    
    @Override
    public void mute() {
        
        volume = MIN_VOLUME;
    }
}
