/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package remotecontrol;

/**
 *
 * @author julius
 */
public interface RemoteControl {
    int MIN_VOLUME = 0;
    int MAX_VOLUME = 100;
    
    boolean powerOnOff();
    int volumeUp(int increment);
    int volumeDown(int decrement);
    void mute();
    
}
