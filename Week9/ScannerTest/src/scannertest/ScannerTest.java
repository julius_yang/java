/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scannertest;

import java.util.Scanner;

/**
 *
 * @author julius
 */
public class ScannerTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Scanner keyboard = new Scanner(System.in);
        Scanner keyboard = (new Scanner(System.in)).useDelimiter(",|\n");
        System.out.println("Enter something:");
        while(keyboard.hasNext()) {
            // Returns true if there is a next token
            String input = keyboard.next();
            input.trim();
            if ( input.equals("Done")) {
                break;
                
            }
            
            // By default the delimiter is whitespace/newline
            System.out.println("The user entered: " + input);
        }
    }
}
