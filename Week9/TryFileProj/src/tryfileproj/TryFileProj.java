/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tryfileproj;

import java.io.File;

/**
 *
 * @author julius
 */
public class TryFileProj {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        File myDir = new File("/Users/julius/JavaTemp");
        //File myDir = new File("JavaTemp");
        //myDir.mkdir();
        System.out.println(myDir + (myDir.isDirectory() ? " is " : " is not ") + "a directory");
        System.out.println(myDir.getName());
        System.out.println(myDir.getPath());
        File myFile = new File(myDir, "File.txt");
        System.out.println(myFile + (myFile.exists() ? " does " : " does not ") + "exist");
        
        File newDir = new File("/Users/julius/JavaTemp/newdir");
        if (!newDir.mkdir()) {
            System.out.println(newDir + " not created");
        }
    }
}
