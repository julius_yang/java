/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package textfileio;

import java.io.*;

/**
 *
 * @author julius
 */
public class TextFileIO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            File source = new File("/Users/julius/myFile.txt");
            File temp = new File("/Users/julius/target.txt");
            
            FileReader fr = new FileReader(source);
            BufferedReader in = new BufferedReader(fr);
            
            FileWriter fw = new FileWriter(temp);
            BufferedWriter out = new BufferedWriter(fw);
            
            String line;
            while ((line = in.readLine()) != null ) {
                System.out.println(line);
                out.append(line, 0, line.length());
                out.newLine();
            }
            
            out.close();
            in.close();
        } 
        catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
