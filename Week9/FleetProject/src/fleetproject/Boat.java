/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

import java.util.Arrays;

/**
 *
 * @author julius
 */
public abstract class Boat implements BoatControl {

    // Instance variables
    private String name;
    private String id;
    String boatState = "at initial velocity";
    int speed;
    boolean power;
    int heading;
    
    // Constructor
    public Boat(String name, String id) {
        this.setName(name);
        this.setId(id);
    }
    
    public Boat(String name) {
        this(name, "none");
    }
    
    // Private methods    
    public int getHeading() {
        return heading;
    }
    
    @Override
    public void powerOn() {
        power = true;
    }
    @Override
    public void powerOff() {
        power = false;
    }
    @Override
    public boolean isPowered() {
        return power;
    }
    String direction() {
 
        if ( heading >= 350 || heading < 10 ) {
            return "NORTH";
        }
        else if ( heading >= 10 && heading < 80 ) {
            return "NORTH EAST";
        }
        else if ( heading >= 80 && heading < 100 ) {
            return "EAST";
        }
        else if ( heading >= 100 && heading < 170 ) {
            return "SOUTH EAST";
        }
        else if ( heading >= 170 && heading < 190 ) {
            return "SOUTH";
        }
        else if ( heading >= 190 && heading < 260 ) {
            return "SOUTH WEST";
        }
        else if ( heading >= 260 && heading < 280 ) {
            return "WEST";
        }
        else if ( heading >= 280 && heading < 350 ) {
            return "NORTH WEST";
        }
        else {
            return "NO HEADING";
        }
    }
    
    private void setName(String name) {
        this.name = name;
    }
    
    final void setId(String id) {
        this.id = id;
    }
    
    // Too lazy to figure out how to retrieve name of class and
    // format it properly (this.getClass().getName() returns
    // the package too)
    String getType() {
        return "Boat";
    }
    
    // Public methods        
    public String getName() {
        return name;
    }
    
    public String getId() {
        return id;
    }
    
    @Override
    public abstract String whatIsBoatState();
    @Override
    public abstract void goFaster();
    @Override
    public abstract void goSlower();
    
    public void normalizeHeading() {
        // insure we are always between 0 and 360
        while ( heading >= 360 ) {
            heading -= 360;
        }
        while ( heading < 0 ) {
            heading += 360;
        }
    }
    
    final boolean isRaceboatName() {
        char firstLetter = name.charAt(0);
        if ( firstLetter == 'B' || firstLetter == 'C' || firstLetter == 'N' ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public int getSpeed() {
        return speed;
    }
    
    public void setSpeed(int speed) {
        if (!this.isPowered()) {
            return;
        }
        this.speed = speed;
    }
    
    @Override
    public String toString() {
        String boatName  = getName();
        // Would be nice to not hardcode a formatting string
        // to pad the id.  Maybe specify a static variable which
        // could then be used to set MAX_BOATS in Fleet() as well
        // as for formatting here.
        String boatId    = String.format("%05d", Integer.parseInt(this.getId()));
        String type      = getType();
        int curSpeed     = getSpeed();
        String state     = whatIsBoatState();  
        String direction = direction();
        
        normalizeHeading();
        
        if ( isPowered() ) {
            return boatName + " (" + boatId + ") is a "
                + type + " at the speed of " + curSpeed + "mph "
                + "(" + state + ") moving " + direction + "(" + heading + " deg)";
        }
        else {
            return boatName + " is powered off.";
        }
    }
}
