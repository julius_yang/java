/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author julius
 */
public class FleetProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Fleet fleet;

        // Boat args:
        // "Sea Monkey" "Backdraft" "Cast Away" "Nautifish" "Destiny"
        try {
            fleet = new Fleet(args);
        } catch (NoBoatsExistException e) {
            System.out.println("NoBoatsExistException: " + e.getMessage());
            return;
        }

        // Can't initialize all boats to speed 8 since
        // they all start unpowered.


        /*
         // It would be a lot easier to pass boat objects
         // around instead of names :(
         for( Boat boat : fleet.getBoats() ) {
         if ( boat == null ) {
         continue;
         }
         String boatName = boat.getName();
         switch (boatName) {
         case "Backdraft":
         fleet.powerOnOff(boatName, 1);
         boat.setSpeed(8);
         fleet.setSpeed(boatName, 10);
         // There is a risk of infinite turning...
         while (boat.getHeading() != 330) {
         fleet.turn(boatName, -1);
         }
         break;
         case "Cast Away":
         fleet.powerOnOff(boatName, 1);  
         boat.setSpeed(8);
         fleet.setSpeed(boatName, 2);
         while (boat.getHeading() != 30) {
         fleet.turn(boatName, 1);
         }
         break;
         case "Nautifish":
         fleet.powerOnOff(boatName, 1);  
         boat.setSpeed(8);
         fleet.setSpeed(boatName, 4);
         while (boat.getHeading() != 90) {
         fleet.turn(boatName, 1);
         }
         break;
         case "Sea Monkey":
         fleet.powerOnOff(boatName, 0);  
         break;
         case "Destiny":
         fleet.powerOnOff(boatName, 1);  
         boat.setSpeed(8);
         fleet.setSpeed(boatName, 15);
         while (boat.getHeading() != 180) {
         fleet.turn(boatName, -1);
         }
         break;
         default:
         fleet.setSpeed(boatName, 0);
         }
         }
         */

        Boat boat = null;
        String command = null;

        Scanner keyboard = (new Scanner(System.in)).useDelimiter(",|\n");
        System.out.println("Enter boat command:");
        while (keyboard.hasNext()) {
            // Returns true if there is a next token
            String input = keyboard.next();
            String trim = input.trim();
            if (trim.equals("Done")) {
                break;
            }

            int boatIndex = fleet.getBoatIndexByName(trim);
            if (boatIndex == -1 && boat == null) {
                // We haven't yet defined a boat, and
                // this input is not an existing boat name
                System.out.println("No such boat name");
                continue;
            } else if (boatIndex != -1 && boat == null) {
                // We were given a legal boat name
                Boat[] boats = fleet.getBoats();
                boat = boats[boatIndex];
                continue;
            } else {
                // This must be a command
                command = trim;
            }

            if (( boat != null && command == null ) ||
                ( boat == null && command != null ) ) {
                // We need both a boat and a command to go on.
                continue;
            }
            // Now we have a boat name and a command.  Let's
            // do something with them.

            // If the boat is powered off, the only legal
            // command is power on.
            if (!boat.isPowered() && !command.equals("power on")) {
                System.out.println("That boat is unpowered.");
                continue;
                //break;
            }
            
            switch (command) {
                case "power on":
                    boat.powerOn();
                    break;
                case "power off":
                    boat.powerOff();
                    break;
                case "speed up":
                    boat.goFaster();
                    break;
                case "slow down":
                    boat.goSlower();
                    break;
                case "turn left":
                    boat.turnLeft();
                    break;
                case "turn right":
                    boat.turnRight();
                    break;
                default:
                    System.out.println("Command not understood.");
                    break;
            }
            
            System.out.println(boat);
            boat = null;
            command = null;
        }
    }
}