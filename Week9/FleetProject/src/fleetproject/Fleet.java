/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

import java.util.Arrays;
import java.lang.Math;
import java.io.*;

/**
 *
 * @author julius
 */
public class Fleet {

    private Boat[] boats;
    private int numberBoats = 0;
    private static int MAX_BOATS;
    private static int POWER_ON;
    private static int POWER_OFF;

    static {
        // Would be nice to tie this to the allowable length of
        // the id somehow.
        MAX_BOATS = 100;
        POWER_ON = 1;
        POWER_OFF = 0;
    }

    public Fleet(String[] args) throws NoBoatsExistException {

        try {

            File source = new File("boats.txt");
            FileReader fr = new FileReader(source);
            BufferedReader in = new BufferedReader(fr);
            
            String line;
            // Get the number of lines in the file
            // This is inefficient since we iterate over the file
            // twice, but it's not too bad for a short file
            int lines = 0;
            while (in.readLine() != null) {
                lines++;
                if (lines == MAX_BOATS) {
                    break;
                }
            }
            in.close();
            
            // If there are no lines, there's nothing to do.
            if (lines == 0) {
                throw new NoBoatsExistException("No boats in input file");
            }
            
            // Declare an array of that size
            String[] allLines = new String[lines];
            
            // Read file again
            fr = new FileReader(source);
            in = new BufferedReader(fr);
            int i = 0;
            while ((line = in.readLine()) != null) {
                allLines[i] = line;
                i++;
                if (i > MAX_BOATS ) {
                    break;
                }
            }
            in.close();       

            for (String name : allLines) {
                numberBoats++;
            }

            // Declare boat holding arrays
            boats = new Boat[numberBoats];
            createBoatsFromArgs(allLines);

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createBoatsFromArgs(String[] args) throws NoBoatsExistException {

        if (args.length == 0) {
            throw new NoBoatsExistException();
        }
        int i = 0;

        // Iterate over args to create boats
        for (String name : args) {
            Boat boat;
            if (isRaceboatName(name)) {
                boat = new RaceBoat(name);
            } else {
                boat = new SailBoat(name);
            }
            boats[i++] = boat;
            assignBoatId(boat);
        }
    }

    private void assignBoatId(Boat boat) {

        // Create an id.  Make sure it hasn't been used.
        String id = createRandomId();
        while (boatHasId(id)) {
            id = createRandomId();
        }

        boat.setId(id);
    }
    // There's probably a java library utility to figure out
    // whether something exists in an array, but I'm lazy

    private boolean boatHasId(String id) {
        boolean isInList = false;
        for (int i = 0; i < boats.length; i++) {

            // Need to check if boats[i] is a boat
            if (boats[i] == null) {
                continue;
            }

            // Is it Boat or a subclass thereof?
            if (!Boat.class.isAssignableFrom(boats[i].getClass())) {
                continue;
            }

            // Check if existing boat has this id
            String existingId = boats[i].getId();
            if (id.equals(existingId)) {
                isInList = true;
                break;
            }
        }
        return isInList;
    }

    // This should be a static boat method
    static boolean isRaceboatName(String name) {
        char firstLetter = name.charAt(0);
        if (firstLetter == 'B' || firstLetter == 'C' || firstLetter == 'N') {
            return true;
        } else {
            return false;
        }
    }

    private String createRandomId() {
        String s;
        int boatId = (int) (MAX_BOATS * Math.random());
        s = Integer.toString(boatId);
        return s;
    }

    public int getBoatIndexByName(String name) {
        Boat boat = null;
        // This method isn't too useful if we have
        // two boats with the same name.  The functions that
        // use this method could easily use the boat object
        // directly instead.
        for (int i = 0; i < boats.length; i++) {
            if (boats[i].getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public void setSpeed(String name, int speed) {
        int boatIndexNumber = getBoatIndexByName(name);

        if (boatIndexNumber == -1) {
            // Can't do anything without a boat
            System.out.println("Error: could not find boat " + name);
            return;
        }

        Boat[] allBoats = getBoats();
        Boat boat = allBoats[boatIndexNumber];

        if (boat == null) {
            return;
        }

        if (!boat.isPowered()) {
            return;
        }

        // Slow the boat down
        if (boat.getSpeed() > speed) {
            do {
                boat.goSlower();
            } while (boat.getSpeed() > speed);
        } // Speed the boat up
        else if (boat.getSpeed() < speed) {
            do {
                boat.goFaster();
            } while (boat.getSpeed() < speed);
        }
    }

    public void setFleetSpeed(int speed) {
        for (int i = 0; i < boats.length; i++) {
            if (boats[i] == null) {
                continue;
            }
            boats[i].setSpeed(speed);
        }

    }

    public Boat[] getBoats() {
        return boats;
    }

    public void turn(String boatName, int direction) {
        int boatIndex = getBoatIndexByName(boatName);
        if (boatIndex == -1) {
            return;
        }
        Boat boat = boats[boatIndex];
        if (boat == null) {
            return;
        }
        if (direction < 0) {
            boat.turnLeft();
        } else if (direction > 0) {
            boat.turnRight();
        }
    }

    public void powerOnOff(String boatName, int powerState) {

        int boatIndex = getBoatIndexByName(boatName);
        if (boatIndex == -1) {
            return;
        }
        Boat boat = boats[boatIndex];
        if (boat == null) {
            return;
        }

        // Why does the assignment require powerState to
        // be an int? 
        if (powerState == POWER_OFF) {
            boat.powerOff();
        } else if (powerState == POWER_ON) {
            boat.powerOn();
        }
    }

    public void displayBoatNames() {
        System.out.println("Race boats:");

        for (int i = 0; i < boats.length; i++) {
            if (boats[i] == null) {
                continue;
            }
//            if ( !boats[i].isPowered()) {
//                System.out.println(boats[i].getName() + " is powered off.");
//            }
            if (boats[i].isRaceboatName()) {
                System.out.println(boats[i]);
            }
        }

        System.out.println();


        System.out.println("Sail boats:");

        for (int i = 0; i < boats.length; i++) {
            if (boats[i] == null) {
                continue;
            }
            if (boats[i].whatIsBoatState().equals("Not underway")) {
                System.out.println(boats[i].getName() + " is powered off.");
            }
            if (!isRaceboatName(boats[i].getName())) {
                System.out.println(boats[i]);
            }
        }
    }
}
