/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package readcommandline;

import java.io.*;
import java.util.Arrays;

/**
 *
 * @author julius
 */
public class ReadCommandLine {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Reader in = new InputStreamReader(System.in);
        char[] cbuf = new char[32];
        Arrays.fill(cbuf,' ');
        try {
            while (in.read(cbuf) != -1) {
                String str = new String(cbuf);
                Arrays.fill(cbuf,' ');
                if (str.contains("quit")) {
                    break;
                }

                System.out.println(str);
            }

            in.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
