/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package arrayexercise;

/**
 *
 * @author julius
 */
public class ArrayExercise {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int num = 0;
        int[][] numbers = new int[15][10];

        for (int row = 0; row < numbers.length; row++) {
            // System.out.println("");
            for (int column = 0; column < numbers[row].length; column++) {
                numbers[row][column] = num;
                //System.out.print(num + " ");
                num++;
            }
        }
        //System.out.println("");
    }
}
