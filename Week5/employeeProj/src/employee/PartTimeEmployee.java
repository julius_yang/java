/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author julius
 */
public class PartTimeEmployee extends Employee{
    PartTimeEmployee(String name, int rate) {
        super(name, rate);
    }
    
    @Override
    // Note this must be public since the overridden method
    // is also public; subclass can't be more restrictive than super
    public double pay(double hrs) {
        if ( hrs < HOURS_PER_WEEK ) {
            income = hrs * hourlyRate;
        }
        else {
            income = (HOURS_PER_WEEK * hourlyRate) + ((hrs - HOURS_PER_WEEK) * hourlyRate * 1.5);
        }
        
        return income;
    }
}
