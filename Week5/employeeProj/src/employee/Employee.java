/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author julius
 */
public class Employee {
    
    private String name;
    final int HOURS_PER_WEEK = 40;
    int hourlyRate;
    double income;
    
    Employee(String name, int hourlyRate) {
        this.name = name;
        this.hourlyRate = hourlyRate;
    }
    
    public String getName() {
        return name;
    }
    
    public double pay(double hrs) {
        income = hrs * hourlyRate;
        return income;
    }
}
