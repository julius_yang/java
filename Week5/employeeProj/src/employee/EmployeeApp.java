/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author julius
 */
public class EmployeeApp {
    public static void main(String[] args) {
        FullTimeEmployee e1 = new FullTimeEmployee("Alin", 50);
        PartTimeEmployee e2 = new PartTimeEmployee("Lucy", 25);
        System.out.println(e1.getName() + "'s pay is " + e1.pay(60));
        System.out.println(e2.getName() + "'s pay is " + e2.pay(60));
    }
}
