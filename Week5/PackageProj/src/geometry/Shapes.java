/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import geometry.shapes2d.Circle;
import geometry.shapes3d.Sphere;

/**
 *
 * @author julius
 */
public class Shapes {
    public static void main(String[] args) {
        Sphere mySphere = new Sphere(10);
        Circle myCircle = new Circle(20);
        
        mySphere.printInfo();
        myCircle.printInfo();
    }
    
}
