/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry.shapes2d;

/**
 *
 * @author julius
 */
public class Circle {
    private int radius;
    
    public Circle(int r) {
        radius = r;
    
    }
    
    public void printInfo() {
        System.out.println("This is a circle with radius " + radius );
    }
}
