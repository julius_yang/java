/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry.shapes3d;

/**
 *
 * @author julius
 */
public class Sphere {
    private int radius;
    
    public Sphere(int r) {
        radius = r;
    }
    
    public void printInfo() {
        System.out.println("This is a sphere with radius " + radius);
    }
}
