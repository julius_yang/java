/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

import java.util.Arrays;

/**
 *
 * @author julius
 */
public class Boat {

    // Instance variables
    private String name;
    private String id;
    String boatState = "at initial velocity";
    
    // Boats are created while moving.  Why?  Because otherwise in displayBoatNames()
    // we have to speed the boat up then slow it back down to get the right message
    // to display, and that is annoying
    int speed;
    static private int boatsCreated;
    static private RaceBoat[] raceBoats;
    static private SailBoat[] sailBoats;
    
    // Constructor
    public Boat(String name, String id) {
        this.setName(name);
        this.setId(id);
    }
    
    // Private methods    
    private void setName(String name) {
        this.name = name;
    }
    
    private void setId(String id) {
        this.id = id;
    }
    
    // Too lazy to figure out how to retrieve name of class and
    // format it properly (this.getClass().getName() returns
    // the package too)
    String getType() {
        return "Boat";
    }
    
    // Public methods        
    public String getName() {
        return name;
    }
    
    public String getId() {
        return id;
    }
    
    public void goFaster() {
        speed++;
    }
    
    public void goSlower() {
        speed--;
    }
    
    public int getSpeed() {
        return speed;
    }
    
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    
    @Override
    public String toString() {
        String boatName  = getName();
        // Would be nice to not hardcode a formatting string
        // to pad the id.  Maybe specify a static variable which
        // could then be used to set MAX_BOATS in Fleet() as well
        // as for formatting here.
        String boatId    = String.format("%05d", Integer.parseInt(this.getId()));
        String type      = getType();
        int curSpeed     = getSpeed();
        String state     = whatIsBoatState();  
        
        return boatName + " (" + boatId + ") is a "
                + type + " at the speed of " + curSpeed + "mph "
                + "(" + state + ")";
    }
    public String whatIsBoatState() {
        return boatState;
    }
}
