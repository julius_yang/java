/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author Julius
 */

public class SailBoat extends Boat {
    
    // variables
    
    // constructors
    SailBoat (String name, String id) {
        super(name, id);
    }

    // private methods
    
    // public methods
    // Too lazy to figure out how to retrieve name of class and
    // format it properly (this.getClass().getName() returns
    // the package too)
    @Override
    String getType() {
        return "Sail Boat";
    }
    
    public void raiseSail() {
        boatState = "Sail Up";
    }
    
    @Override
    public void goFaster() {
        super.goFaster();
        raiseSail();
    }

    public void lowerSail() {
        boatState = "Sail Down";
    }
    
    @Override
    public void goSlower() {
        super.goSlower();
        lowerSail();
    }
    
}