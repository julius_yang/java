/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;
import java.util.Arrays;
import java.lang.Math;

/**
 *
 * @author julius
 */
public class Fleet {
    
    private Boat[] boats;
    private Boat[] raceboats;
    private Boat[] sailboats;
    
    private String[] ids;
    private int numberRaceboats = 0;
    private int numberSailboats = 0;
    private int numberBoats = 0;
    
    private static int MAX_BOATS;
    
    static {
        // Would be nice to tie this to the allowable length of
        // the id somehow.
        MAX_BOATS = 99999;
    }
    
    public Fleet(String[] args) {
                
        // array of existing boat ids
        ids = new String[args.length];
        
        // Prepopulate the array of ids since we will be doing comparisons with
        // all elements in it
        Arrays.fill(ids, "");

        // Determine how many raceboats vs. sailboats we need so
        // we can declare array sizes
        for ( String name : args ) {
            if ( isRaceboatName(name) ) {
                numberRaceboats++;
            }
            else {
                numberSailboats++;
            }
            numberBoats++;
        }
        
        // If we don't limit the number of boats and someone
        // enters MAX_BOATS+1 boats we'll run out of ids and enter
        // an infinite loop.  Allow only the first MAX_BOATS boats.
        // Ideally we'd also make sure raceboats + sailboats is
        // less than MAX_BOATS.
        numberBoats = Math.min(numberBoats, MAX_BOATS);
        numberRaceboats = Math.min(numberRaceboats, MAX_BOATS);
        numberSailboats = Math.min(numberSailboats, MAX_BOATS);
        
        // Declare boat holding arrays
        raceboats = new RaceBoat[numberRaceboats];
        sailboats = new SailBoat[numberSailboats];
        boats     = new Boat[numberBoats];
        
        // Declare some iteration control variables
        int raceboatIndex = 0;
        int sailboatIndex = 0;
        int boatIndex = 0;
        int idIndex = 0;
        
        // Iterate over args to create boats
        for ( String name : args ) {
            String id;
            
            id = createRandomId();
            while ( isInBoatIdList( id ) ) {
                id = createRandomId();
            }

            // Remember this id has been used
            ids[idIndex++] = id;

            // populate boat arrays
            if ( isRaceboatName(name) ) {
                Boat boat = new RaceBoat(name,id);
                raceboats[raceboatIndex++] = boat;
                boats[boatIndex++] = boat;
            }
            else {
                Boat boat = new SailBoat(name,id);
                sailboats[sailboatIndex++] = boat;
                boats[boatIndex++] = boat;
            } 
        }
    }
 
    // There's probably a java library utility to figure out
    // whether something exists in an array, but I'm lazy
    private boolean isInBoatIdList(String id) {
        boolean isInList = false;
        for (int i=0; i < ids.length; i++) {
            String existingId = ids[i];
            if (id.equals(existingId)) {
                isInList = true;
                break;
            }
        }
        return isInList;
    }
    
    private boolean isRaceboatName(String name) {
        char firstLetter = name.charAt(0);
        if ( firstLetter == 'B' || firstLetter == 'C' || firstLetter == 'N' ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private String createRandomId() {
        String s;
        int boatId = (int)(MAX_BOATS * Math.random());
        s = Integer.toString(boatId);
        return s;
    }

    public Boat getBoatByName(String name) {
        Boat boat = null;
        // This method isn't too useful if we have
        // two boats with the same name.  The functions that
        // use this method could easily use the boat object
        // directly instead.
        for (Boat b : getBoats() ) {
            if ( b.getName().equals(name) ) {
                return b;
            }
        }
        return boat;
    }

    public void setSpeed(String name, int speed) {
        Boat boat = getBoatByName(name);
        
        // Can't do anything without a boat
        if ( boat == null ) {
            System.out.println("Error: boat is null");
            return;
        }
        // Slow the boat down
        if ( boat.getSpeed() > speed ) {
            do {
                boat.goSlower();
            } while ( boat.getSpeed() > speed );
        }
        // Speed the boat up
        else if ( boat.getSpeed() < speed ) {
            do {
                boat.goFaster();            
            } while ( boat.getSpeed() < speed );
        }
    }
    
    public void setFleetSpeed(int speed) {
        for (int i = 0; i < boats.length; i++ ) {
            boats[i].setSpeed(speed);
        }
        
    }
    
    public Boat[] getBoats() {
        return boats;
    }
    
    public void displayBoatNames() {
        
        System.out.println("Race boats:");
        for ( Boat boat : raceboats ) {
            System.out.println(boat);
        }
        
        System.out.println();
        
        System.out.println("Sail boats:");
        for ( Boat boat : sailboats ) {
            System.out.println(boat);
        }
    }
}
