/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author Julius
 */
public class RaceBoat extends Boat {
        
    // variables
    
    // constructors
    RaceBoat (String name, String id) {
        super(name, id);
    }
    
    // private methods
    
    // public methods
    
    // Too lazy to figure out how to retrieve name of class and
    // format it properly (this.getClass().getName() returns
    // the package too)
    @Override
    String getType() {
        return "Race Boat";
    }
    
    public void throttleForward() {
        boatState = "Throttle Forward";
    }
    
    @Override
    public void goFaster() {
        super.goFaster();
        throttleForward();
    }

    public void throttleBack() {
        boatState = "Throttle Back";
    }
    
    @Override
    public void goSlower() {
         super.goSlower();
         throttleBack();
    }
    
}
