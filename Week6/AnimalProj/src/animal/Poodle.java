/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author julius
 */
public class Poodle extends Dog {
    Poodle(String name) {
        super(name); // Dog constructor takes only one parameter
    }
    
    @Override
    void sound() {
        System.out.println(name + " sounds bark bark");
    }
}
