/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author julius
 */
public class PetHome {
    public static void main(String[] args) {
        Animal[] pets = {
            new Dog("Cho"),
            new Cat("Cookie"),
            new Duck("Donald"),
            new Poodle("Luna")
        };
        
        for (int i = 0; i < pets.length ; i++ ) {
            pets[i].sound();
            pets[i].feed();
        }

        Poodle aPoodle = new Poodle("Simba");
        Dog aDog = aPoodle;
        
        /*
        aDog.sound(); // Poodle sounds
        
        Animal anAnimal = aPoodle;
        anAnimal.sound(); // Poodle sounds
        
        aPoodle = (Poodle) pets[3]; // pets is an array of Animal so downward casting is required
        
        aPoodle = (Poodle) pets[0]; // illegal because poodle variable is trying to reference a dog object
        */
        
        // if aPoodle were a Dog then this is legal:
        // aPoodle = (Dog) pets[0];
        
        // Poodle aPoodle = new Poodle();
        // Dog aDog = aPoodle
        // if Poodle has a method 'groom' then you can't call aDog.groom() as it stands
        // they are not polymorphic
        // However you can cast aDog to Poodle THEN call groom
        // ((Poodle) aDog).groom();
        
        // if ( pet.getClass() == Dog.class )
        // Object has getClass method and .class is common to all classes
        
        for (int i = 0; i < pets.length ; i++ ) {
            System.out.println(pets[i]);
        }
        
        aDog = (Dog) pets[0];
        if ( aDog.equals(pets[0])) {
            System.out.println(aDog + " is equal to " + pets[0]);
        }
        
    }
}
