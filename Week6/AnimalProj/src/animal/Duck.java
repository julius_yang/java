/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author julius
 */
public class Duck extends Animal{
    Duck(String name) {
        super(name, "Duck");
    }
    
    @Override
    void sound() {
        System.out.println(name + " sounds quack quack");
    }
    
    void feed() {
        System.out.println(name + " eats grain");
    }
}
