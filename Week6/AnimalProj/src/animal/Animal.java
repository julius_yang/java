/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author julius
 */
public abstract class Animal {
    public String name;
    public String type;
    
    Animal( String name, String type ) {
        this.name = name;
        this.type = type;
    }
    
    abstract void sound();
    abstract void feed();
}
