/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author julius
 */
public class Cat extends Animal {
    Cat ( String name ) {
        super(name, "Cat");
    }
    
    @Override
    void sound() {
        System.out.println(name + " sounds mew mew");
    }
    
    @Override
    void feed() {
        System.out.println(name + " eats fish");
    }
}
