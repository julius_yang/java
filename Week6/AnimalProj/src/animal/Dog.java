/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author julius
 */
public class Dog extends Animal {
    Dog(String name) {
        super(name, "Dog");
    }
    
    @Override
    void sound() {
        System.out.println(name + " sounds woof woof");
    }
    
    @Override
    void feed() {
        System.out.println(name + " eats meat");
        
    }
    
    @Override
    public String toString() {
        return "I am a Dog and my name is " + name;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if ( o.getClass() != Dog.class) {
            return false;
        }
        if (((Dog)o).name.equals(name) && ((Dog)o).type.equals(type)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
}
