/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;
import java.util.Arrays;
import java.lang.Math;

/**
 *
 * @author julius
 */
public class Fleet {
    
    private Boat[] boats;
    private int numberBoats = 0;
    
    private static int MAX_BOATS;
    
    static {
        // Would be nice to tie this to the allowable length of
        // the id somehow.
        MAX_BOATS = 99999;
    }
    
    public Fleet(String[] args) {
                
        for ( String name : args ) {
            numberBoats++;
        }
        
        // If we don't limit the number of boats and someone
        // enters MAX_BOATS+1 boats we'll run out of ids and enter
        // an infinite loop.  Allow only the first MAX_BOATS boats.
        numberBoats = Math.min(numberBoats, MAX_BOATS);
        
        // Declare boat holding arrays
        boats     = new Boat[numberBoats];
        
        createBoatsFromArgs(args);
    }
    
    private void createBoatsFromArgs(String[] args) {
        int i = 0;

        // Iterate over args to create boats
        for ( String name : args ) {
            Boat boat;
            if ( isRaceboatName(name) ) {
                boat = new RaceBoat(name);
            }
            else {
                boat = new SailBoat(name);
            }
            boats[i++] = boat;
            assignBoatId(boat);
        }
    }
 
    private void assignBoatId(Boat boat) {

        // Create an id.  Make sure it hasn't been used.
        String id = createRandomId();
        while ( boatHasId( id ) ) {
            id = createRandomId();
        }
        
        boat.setId(id);
    }
    // There's probably a java library utility to figure out
    // whether something exists in an array, but I'm lazy
    private boolean boatHasId(String id) {
        boolean isInList = false;
        for (int i=0; i < boats.length; i++) {
            
            // Need to check if boats[i] is a boat
            if ( boats[i] == null ) {
                continue;
            }
            
            // Is it Boat or a subclass thereof?
            if ( !Boat.class.isAssignableFrom(boats[i].getClass() )) {
                continue;
            }
            
            // Check if existing boat has this id
            String existingId = boats[i].getId();
            if (id.equals(existingId)) {
                isInList = true;
                break;
            }
        }
        return isInList;
    }
    
    // This should be a static boat method
    private boolean isRaceboatName(String name) {
        char firstLetter = name.charAt(0);
        if ( firstLetter == 'B' || firstLetter == 'C' || firstLetter == 'N' ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private String createRandomId() {
        String s;
        int boatId = (int)(MAX_BOATS * Math.random());
        s = Integer.toString(boatId);
        return s;
    }

    public int findBoatName(String name) {
        Boat boat = null;
        // This method isn't too useful if we have
        // two boats with the same name.  The functions that
        // use this method could easily use the boat object
        // directly instead.
        for (int i = 0 ; i < boats.length ; i++ ) {
            if ( boats[i].getName().equals(name) ) {
                return i;
            }
        }
        return -1;
    }

    public void setSpeed(String name, int speed) {
        int boatIndexNumber = findBoatName(name);
        
        if ( boatIndexNumber == -1 ) {
            // Can't do anything without a boat
            System.out.println("Error: could not find boat " + name);
            return;
        }
        
        Boat[] allBoats = getBoats();
        Boat boat = allBoats[boatIndexNumber];
        
        // Slow the boat down
        if ( boat.getSpeed() > speed ) {
            do {
                boat.goSlower();
            } while ( boat.getSpeed() > speed );
        }
        // Speed the boat up
        else if ( boat.getSpeed() < speed ) {
            do {
                boat.goFaster();            
            } while ( boat.getSpeed() < speed );
        }
    }
    
    public void setFleetSpeed(int speed) {
        for (int i = 0; i < boats.length; i++ ) {
            if( boats[i] == null ) {
                continue;
            }
            boats[i].setSpeed(speed);
        }
        
    }
    
    public Boat[] getBoats() {
        return boats;
    }
    
    public void displayBoatNames() {
        System.out.println("Race boats:");

        for ( int i = 0; i < boats.length; i++ ) {
            if ( boats[i] == null ) {
                continue;
            }
            
            if ( boats[i].isRaceboatName() ) {
                System.out.println(boats[i]);
            }
        }
        
        System.out.println();
        
        
        System.out.println("Sail boats:");

        for ( int i = 0; i < boats.length; i++ ) {
            if ( boats[i] == null ) {
                continue;
            }
            
            if ( !isRaceboatName(boats[i].getName()) ) {
                System.out.println(boats[i]);
            }
        }
    }
}
