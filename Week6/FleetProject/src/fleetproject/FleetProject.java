/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author julius
 */
public class FleetProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fleet fleet = new Fleet(args);
        
        // Initialize all boat speeds so that the output
        // matches what is required for the homework
        fleet.setFleetSpeed(8);

        // Set boat speeds based on name...this should really
        // be done based on the object
        for( Boat boat : fleet.getBoats() ) {
            if ( boat == null ) {
                continue;
            }
            String boatName = boat.getName();
            switch (boatName) {
                case "Backdraft":
                    fleet.setSpeed(boatName, 10);
                    break;
                case "Cast Away":
                    fleet.setSpeed(boatName, 2);
                    break;
                case "Nautifish":
                    fleet.setSpeed(boatName, 4);
                    break;
                case "Sea Monkey":
                    fleet.setSpeed(boatName, 7);
                    break;
                case "Destiny":
                    fleet.setSpeed(boatName, 15);
                    break;
                default:
                    fleet.setSpeed(boatName, 0);
            }
        }
        
        fleet.displayBoatNames();
        
    }
}
