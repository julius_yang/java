/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

import java.util.Arrays;

/**
 *
 * @author julius
 */
public abstract class Boat {

    // Instance variables
    private String name;
    private String id;
    String boatState = "at initial velocity";
    int speed;

    
    // Constructor
    public Boat(String name, String id) {
        this.setName(name);
        this.setId(id);
    }
    
    public Boat(String name) {
        this(name, "none");
    }
    
    // Private methods    
    private void setName(String name) {
        this.name = name;
    }
    
    final void setId(String id) {
        this.id = id;
    }
    
    // Too lazy to figure out how to retrieve name of class and
    // format it properly (this.getClass().getName() returns
    // the package too)
    String getType() {
        return "Boat";
    }
    
    // Public methods        
    public String getName() {
        return name;
    }
    
    public String getId() {
        return id;
    }
    
    public abstract String whatIsBoatState();
    public abstract void goFaster();
    public abstract void goSlower();
    
    final boolean isRaceboatName() {
        char firstLetter = name.charAt(0);
        if ( firstLetter == 'B' || firstLetter == 'C' || firstLetter == 'N' ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public int getSpeed() {
        return speed;
    }
    
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    
    @Override
    public String toString() {
        String boatName  = getName();
        // Would be nice to not hardcode a formatting string
        // to pad the id.  Maybe specify a static variable which
        // could then be used to set MAX_BOATS in Fleet() as well
        // as for formatting here.
        String boatId    = String.format("%05d", Integer.parseInt(this.getId()));
        String type      = getType();
        int curSpeed     = getSpeed();
        String state     = whatIsBoatState();  
        
        return boatName + " (" + boatId + ") is a "
                + type + " at the speed of " + curSpeed + "mph "
                + "(" + state + ")";
    }
}
