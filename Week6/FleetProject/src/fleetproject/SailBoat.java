/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author Julius
 */

public class SailBoat extends Boat {
    
    // variables
    
    // constructors
    SailBoat (String name, String id) {
        super(name, id);
    }

    SailBoat (String name) {
        super(name);
    }
    // private methods
    
    // public methods
    // Too lazy to figure out how to retrieve name of class and
    // format it properly (this.getClass().getName() returns
    // the package too)
    @Override
    String getType() {
        return "Sail Boat";
    }
    
    public void raiseSail() {
        boatState = "Sail Up";
    }
    
    @Override
    public void goFaster() {
        speed++;
        raiseSail();
    }
    
    @Override
    public void goSlower() {
        speed--;
        lowerSail();
    }
    

    public void lowerSail() {
        boatState = "Sail Down";
    }
    
    @Override
    public String whatIsBoatState() {
        return boatState;
    }
}