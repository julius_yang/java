/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testexceptionproj2;

/**
 *
 * @author julius
 */
public class TestExceptionProj2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            foo();
        } catch (Exception e) {
            System.out.println("Caught exception in main: " + e.getMessage());
        } finally {
            System.out.println("finally clause from main");
        }
    }
    
    static void foo() throws Exception {
        try {
            Exception f = new Exception("foo exception");
            throw f;
            // This throw is caught by the next catch block.  You
            // don't normally catch right when you throw.
            
            // If you return instead of throw, then the finally clause
            // is still executed
        } catch (Exception e) {
            System.out.println("caught exception in foo: " + e.getMessage());
        } finally {
            System.out.println("Finally clause from foo");
        }
    }
    
}
