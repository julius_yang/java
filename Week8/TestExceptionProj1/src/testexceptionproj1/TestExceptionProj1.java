/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testexceptionproj1;

/**
 *
 * @author julius
 */
public class TestExceptionProj1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        foo();
        System.out.println("main completed");
    }
    
    static void foo() {
        try {
            bar();
            System.out.println("bar method copleted");
        } catch (Exception e) {
            System.out.println("Caught Exception in foo: " + e.getMessage());          
        }
    }
    
    // You can also re-throw
    // static void foo() throws Exception {
    //      bar();
    // }
    
    static void bar() throws Exception {
        Exception f = new Exception("bar exception");
        throw f;
    }
}
