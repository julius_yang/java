/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author julius
 */
public class NoBoatsExistException extends Exception {
    
    NoBoatsExistException() {
        super();
    }
    NoBoatsExistException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return "No boat names were supplied\n" + super.getMessage();
    }

}
