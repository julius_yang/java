/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author julius
 */
public class FleetProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Fleet fleet;
        
        // Boat args:
        // "Sea Monkey" "Backdraft" "Cast Away" "Nautifish" "Destiny"
        try {
            fleet = new Fleet(args);
        } catch (NoBoatsExistException e) {
            System.out.println("NoBoatsExistException: " + e.getMessage());
            return;
        }
        
        // Can't initialize all boats to speed 8 since
        // they all start unpowered.
        
        // It would be a lot easier to pass boat objects
        // around instead of names :(
        for( Boat boat : fleet.getBoats() ) {
            if ( boat == null ) {
                continue;
            }
            String boatName = boat.getName();
            switch (boatName) {
                case "Backdraft":
                    fleet.powerOnOff(boatName, 1);
                    boat.setSpeed(8);
                    fleet.setSpeed(boatName, 10);
                    // There is a risk of infinite turning...
                    while (boat.getHeading() != 330) {
                        fleet.turn(boatName, -1);
                    }
                    break;
                case "Cast Away":
                    fleet.powerOnOff(boatName, 1);  
                    boat.setSpeed(8);
                    fleet.setSpeed(boatName, 2);
                    while (boat.getHeading() != 30) {
                        fleet.turn(boatName, 1);
                    }
                    break;
                case "Nautifish":
                    fleet.powerOnOff(boatName, 1);  
                    boat.setSpeed(8);
                    fleet.setSpeed(boatName, 4);
                    while (boat.getHeading() != 90) {
                        fleet.turn(boatName, 1);
                    }
                    break;
                case "Sea Monkey":
                    fleet.powerOnOff(boatName, 0);  
                    break;
                case "Destiny":
                    fleet.powerOnOff(boatName, 1);  
                    boat.setSpeed(8);
                    fleet.setSpeed(boatName, 15);
                    while (boat.getHeading() != 180) {
                        fleet.turn(boatName, -1);
                    }
                    break;
                default:
                    fleet.setSpeed(boatName, 0);
            }
        }
        
        fleet.displayBoatNames();
        
    }
}
