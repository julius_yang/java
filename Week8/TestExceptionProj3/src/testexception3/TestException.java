/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testexception3;

/**
 *
 * @author julius
 */
public class TestException extends Exception {
    TestException() {
        super();
    }
    TestException(String s) {
        super(s);
    }
    
    @Override
    public String getMessage() {
        return "This is Test Exception: " + super.getMessage();
    }
}
