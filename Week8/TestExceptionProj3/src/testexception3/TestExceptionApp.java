/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testexception3;

/**
 *
 * @author julius
 */
public class TestExceptionApp {
    public static void main(String[] args) {
        for (int i = 0 ; i < args.length; i++ ) {
            try {
            thrower(args[i]);
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
            } catch (TestException e) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    static void thrower(String s) throws TestException {
        switch (s) {
            case "divide":
                int i = 0;
                int j = i/i; // This will throw a runtime exception which we
                             // do not need to catch
                break;
            case "test":
                throw new TestException("Test message exception");
                //break;
                
        }
    }
}
