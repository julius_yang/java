/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

/**
 *
 * @author julius
 */
public class FleetProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fleet fleet = new Fleet(args);
        fleet.displayBoatNames();
        
        System.out.println("\nTesting findBoatName on 'Nautifish'");
        fleet.findBoatName("Nautifish");
        System.out.println("\nTesting findBoatName on 'Exxon Valdez'");
        fleet.findBoatName("Exxon Valdez");
    }
}
