/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;

import java.util.Arrays;

/**
 *
 * @author julius
 */
public class Boat {

    // Instance variables
    private String name;
    private boolean sailUp;
    private String id;
    static private int boatsCreated;
    
    //Should have defined this on the fleet
    final static private String[] ids;
    final static private int MAX_BOATS;
    
    static {
        boatsCreated = 0;
        MAX_BOATS = 99999;
        ids = new String[MAX_BOATS];
        Arrays.fill(ids, "");
    }
    
    // Constructor
    public Boat(String nameArg) {
        this.setName(nameArg);
        // id = createId(boatsCreated);
        id = createRandomId();
        ids[boatsCreated] = id;
        boatsCreated++;
    }
    
    // Private methods
    
    // Create sequential ids.  I realize you probably
    // want us to generate a random 5 digit id and
    // check to see if it exists...
    private String createId(int i) {
        //Make sure we are limited to 5 digits
        if (i > MAX_BOATS) {
            System.out.println("Error: ran out of boat ids!");
            System.exit(0);
        }
        //zero pad the id
        return String.format("%05d", i);
    }
    
    //...so I do that here.  I prefer sequential though.
    private String createRandomId() {
        String s;
        boolean idExists = false;
        do {
          int boatId = (int)(MAX_BOATS * Math.random());
          s = Integer.toString(boatId);
          // Look through ids for this string.
          // Seems horribly inefficient to iterate
          // through MAX_BOATS array each time we generate
          // an id though.  Maybe there is a way to only
          // generate an array 'ids' with the length of
          // main()'s args?
          for ( int i = 0; i < ids.length; i++ ){
              if ( ids[i].equals(s) ) {
                  idExists = true;
              }
          }
        } while ( idExists );
        
        return s;
    }
    
    private void goFast() {
        sailUp = true;
    }
    
    private void goSlow() {
        sailUp = false;
    }
    
    private void setName(String argName) {
        name = argName;
        char firstLetter = name.charAt(0);
        switch (firstLetter) {
            case 'B':
                this.goFast();
                break;
            case 'C':
                this.goFast();
                break;
            case 'N':
                this.goFast();
                break;
            default:
                this.goSlow();
                break;
        }
    }
    
    // Public methods
    public String getName() {
        return name;
    }
    
    public String getId() {
        return id;
    }
    public String sailAction() {
        String sailState = sailUp ? "raise" : "lower";
        return sailState + " the sail.";
    }
}
