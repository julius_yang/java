/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fleetproject;
import java.util.Arrays;

/**
 *
 * @author julius
 */
public class Fleet {
    
    final private Boat[] boats;
    
    public Fleet(String[] args) {
        boats = new Boat[args.length];
        
        int i = 0;
        for ( String name : args ) {
            Boat boat = new Boat(name);
            boats[i++] = boat;
        }
    }
    
    public void displayBoatNames() {
        for ( Boat boat : boats ) {
            String name = boat.getName();
            String id   = String.format("%05d", Integer.parseInt(boat.getId()));
            String sail = boat.sailAction();
            
            String s = name + " (" + id + ") " + sail;
            System.out.println(s);
        }
    }
    
    public void findBoatName(String name) {
        String s;
        boolean boatFound = false;
        for (Boat boat : boats ) {
            if ( boat.getName().equals(name) ) {
                s = boat.getName() + " boat ID is " + boat.getId();
                System.out.println(s);
                boatFound = true;
                break;
            }
        }
        
        if (!boatFound) {
            s = name + " does not exist in the boats list.";
            System.out.println(s);
        }
    }
}
