/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rectangle;

/**
 *
 * @author julius
 */
public class Point {
    private double x;
    private double y;
    
    static final private double MAX_POINT_COORDINATE = 10.0;
    
    Point(double x, double y) {
        if (x < MAX_POINT_COORDINATE && x > -MAX_POINT_COORDINATE) {
            this.x = x;
        }
        else {
            System.out.println("Invalid x coordinate " + x);
        }
        
        if (y < MAX_POINT_COORDINATE && y > -MAX_POINT_COORDINATE) {
            this.y = y;
        }
        else {
            System.out.println("Invalid y coordinate " + y);
        }
    }
    
    //copy constructor -- takes object of its own type
    Point(Point point) {
        if ( point.x < MAX_POINT_COORDINATE && point.x > -MAX_POINT_COORDINATE) {
            this.x = point.x;
        }
        else {
            System.out.println("Invalid x coordinate " + point.x);
        }
        
        if (point.y < MAX_POINT_COORDINATE && point.y > -MAX_POINT_COORDINATE) {
            this.y = point.y;
        }
        else {
            System.out.println("Invalid y coordinate " + point.y);
        }
    }
    
    double getX() {
        return x;
    }
    
    double getY() {
        return y;
    }
}
