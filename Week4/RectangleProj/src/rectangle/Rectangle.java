/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rectangle;

/**
 *
 * @author julius
 */
public class Rectangle {
    private Point tl;
    private Point br;
    private double area;
    
    Rectangle(double x1, double x2, double y1, double y2) {
        //tl = new Point(Math.min(x1,x2), Math.max(y1,y2) );
        //br = new Point(Math.max(x1,x2), Math.min(y1,y2) );
        
        // Example of chaining constructors using this keyword
        this( new Point(Math.min(x1,x2), Math.max(y1,y2)), 
              new Point(Math.max(x1,x2), Math.min(y1,y2)));
        
    }
    
    Rectangle(Point point1, Point point2) {
        tl = point1;
        br = point2;
    }
    
    void calculateArea() {
        
        double width  = br.getX() - tl.getX();
        double height = tl.getY() - br.getY();
        
        area = width * height;
        System.out.println("the area of the rectangle is " + area);
    }
    
    public static void main(String[] args) {
        if (args.length != 4) {
            System.out.println("Please provide 4 numbers");
            return;
        }
        
        double x1 = Double.parseDouble(args[0]);
        double x2 = Double.parseDouble(args[1]);
        double y1 = Double.parseDouble(args[2]);
        double y2 = Double.parseDouble(args[3]);
        
        Rectangle r1 = new Rectangle(x1,x2,y1,y2);
        //r1.setPoints(x1,x2,y1,y2);
        r1.calculateArea();
    }
}
